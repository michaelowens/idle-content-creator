import store from '../store'

export const FPS = 10

export class Game {
  lastLoopTime = 0

  constructor() {
    store.dispatch('loadActions')

    // render()
    this.lastLoopTime = +new Date()
    setInterval(() => this.loop(), 1000 / FPS)
    setInterval(() => this.autosave(), 1000 * 30)
  }

  loop() {
    const delta = +new Date() - this.lastLoopTime
    store.dispatch('setDelta', store.state.player.deltaTime + delta)
    this.upgrades()
    this.lastLoopTime = +new Date()
  }

  upgrades() {
    const { player, upgrades } = store.state
    player.mps = 0
    Object.keys(upgrades).forEach(name => {
      const upgrade = upgrades[name]
      if (upgrade.quantity < 1) return
      player.mps += upgrade.value * upgrade.quantity
      store.dispatch('updatePlayer', player)
      store.dispatch('increaseMoney', (upgrade.value * upgrade.quantity) / FPS)
    })
  }

  autosave() {
    store.dispatch('savePlayer')
    store.dispatch('saveUpgrades')
  }
}
