import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import player from './player'
import upgrades from './upgrades'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    actions,
    player,
    upgrades,
  },
})
