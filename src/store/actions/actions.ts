import * as type from './types'
import { LOCAL_UPGRADES } from '../../constants'

function getActionsConfig() {
  let actions = require('../../actions.json')

  Object.keys(actions).forEach(key => {
    actions[key].lastRunDelta = 0
  })

  return actions
}

function getLocalUpgrades() {
  return localStorage.getItem(LOCAL_UPGRADES)
}

const actions = {
  async loadActions({ commit }) {
    console.log('in loadActions')
    const actions = getActionsConfig()

    commit(type.LOAD_ACTIONS, actions)
  },

  async getUpgrades({ commit }) {
    const storedUpgrades = getLocalUpgrades()

    // commit(type.GET_UPGRADES, storedUpgrades)
  },

  async runAction({ commit, state, rootState, dispatch }, { name, delta }) {
    const action = state[name]
    const availableAt = action.lastRunDelta + action.duration
    if (+new Date() >= availableAt) {
      commit(type.RUN_ACTION, { name, delta })
      // dispatch('actionCompleted', player) after action is done?
    }
  },

  saveUpgrades({ state }) {
    localStorage.setItem(LOCAL_UPGRADES, JSON.stringify(state))
  },
}

export default actions
