import * as type from './types'
import actions from './actions'

const state = {}

const mutations = {
  [type.LOAD_ACTIONS](state, data) {
    console.log('in LOAD_ACTIONS mutation')

    if (data !== null) {
      Object.assign(state, data)
    }
  },

  [type.RUN_ACTION](state, { name, delta }) {
    state[name].lastRunDelta = delta
  },
}

export default {
  state,
  mutations,
  actions,
}
