import * as type from './types'
import actions from './actions'

const state = {
  money: 0,
  totalMoney: 0,
  mps: 0,
  deltaTime: 0,
}

const mutations = {
  [type.GET_PLAYER](state, action) {
    if (action.player !== null) {
      const storedPlayer = JSON.parse(action.player)

      Object.assign(state, storedPlayer)
    }
  },

  [type.UPDATE_PLAYER](state, action) {
    Object.assign(state, action.player)
  },

  [type.INCREASE_MONEY](state, amount) {
    state.money += amount
    state.totalMoney += amount
  },

  [type.SET_DELTA](state, amount) {
    state.deltaTime = amount
  },
}

const getters = {}

export default {
  state,
  mutations,
  actions,
  getters,
}
