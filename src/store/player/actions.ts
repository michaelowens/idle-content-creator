import * as type from './types'
import { LOCAL_PLAYER } from '../../constants'

function getLocalPlayer() {
  return localStorage.getItem(LOCAL_PLAYER)
}

const actions = {
  async getPlayer({ commit }) {
    const storedPlayer = getLocalPlayer()

    commit(type.GET_PLAYER, {
      player: storedPlayer,
    })
  },

  updatePlayer({ commit }, player) {
    commit(type.UPDATE_PLAYER, {
      player,
    })
  },

  savePlayer({ state }) {
    localStorage.setItem(LOCAL_PLAYER, JSON.stringify(state))
  },

  increaseMoney({ commit }, amount) {
    commit(type.INCREASE_MONEY, amount)
  },

  setDelta({ commit }, value) {
    commit(type.SET_DELTA, value)
  },
}

export default actions
