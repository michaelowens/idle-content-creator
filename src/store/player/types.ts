export const GET_PLAYER = 'GET_PLAYER'
export const UPDATE_PLAYER = 'UPDATE_PLAYER'
export const INCREASE_MONEY = 'INCREASE_MONEY'
export const SET_DELTA = 'SET_DELTA'
