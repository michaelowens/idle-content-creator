import * as type from './types'
import actions from './actions'

const state = {
  hobby: {
    name: 'Hobby videos for family',
    cost: 1,
    increase: 1.15,
    value: 0.1,
    quantity: 0,
    unlocksAt: 0,
  },
  videoEditor: {
    name: 'Hire Video Editor',
    cost: 50,
    increase: 1.2,
    value: 0.5,
    quantity: 0,
    unlocksAt: 1,
  },
}

const mutations = {
  [type.GET_UPGRADES](state, data) {
    if (data !== null) {
      const storedUpgrades = JSON.parse(data)

      Object.assign(state, storedUpgrades)
    }
  },

  [type.BUY_UPGRADE](state, name) {
    state[name].quantity++
    state[name].cost *= state[name].increase
  },
}

export default {
  state,
  mutations,
  actions,
}
