import * as type from './types'
import { LOCAL_UPGRADES } from '../../constants'

function getLocalUpgrades() {
  return localStorage.getItem(LOCAL_UPGRADES)
}

const actions = {
  async getUpgrades({ commit }) {
    const storedUpgrades = getLocalUpgrades()

    commit(type.GET_UPGRADES, storedUpgrades)
  },

  async buyUpgrade({ commit, state, rootState, dispatch }, name) {
    const { player } = rootState
    const upgrade = state[name]
    if (player.money >= upgrade.cost) {
      player.money -= upgrade.cost
      commit(type.BUY_UPGRADE, name)
      dispatch('updatePlayer', player)
    }
  },

  saveUpgrades({ state }) {
    localStorage.setItem(LOCAL_UPGRADES, JSON.stringify(state))
  },
}

export default actions
