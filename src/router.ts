import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './ui/pages/Home.vue'
// import store from './store'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.NODE_ENV === 'production' ? '/idle-content-creator/' : '/',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      component: Home,
    },
  ],
})

export default router
