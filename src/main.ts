import Vue from 'vue'
import App from './ui/App.vue'
import router from './router'
import store from './store'
import { Game } from './game'

new Game()

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})
